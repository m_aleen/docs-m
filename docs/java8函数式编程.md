---
tetle: Java 8 函数式编程
---

###  Java 8 函数式编程
java8函数式核心概念
- 函数接口（Funtion）
- 流（stream）
- 聚合器（Collector）

#### 函数接口
- 函数接口是行为的抽象
- 函数接口是数据转换器
##### 四大基础函数接口
- Supplier ：数据提供器，没有输入参数， 输出为类型T
- Function：数据转换器，接收一个 T 类型的对象，返回一个 R类型的对象，提供apply，compose，andThen，identity
- Consumer：数据消费器，  接收一个 T类型的对象，无返回值，通常用于根据T对象做些处理，提供accept，andThen
- Predicate：条件测试器，条件测试器，接收一个 T 类型的对象，返回布尔值，通常用于传递条件函数，提供了 test (条件测试) , and-or- negate(与或非) 方法
##### 扩展函数接口
- 参数扩展：接受双参数Bi前缀
- 类型扩展：接受原子类型参数
- 特殊变形：BinaryOperator

##### 函数接口接入参数
- 类/对象的静态方法引用，实例方法引用(PS: 使用::)
- 类的构造器（PS: Class::new）
- lambda表达式

#### 聚合器
流的最后都是使用Collect来进行数据聚合，返回数据
##### Reduce
Reduce推导需要的是三个重要的参数
- init初始数据
- 二元操作符BinaryOperator
- 聚合结果源s
```java
    int num1 = list.stream().reduce(0, (a, b) -> a + b);
```
##### 聚合器四要素
- 容器的初始化（Supplier）
- 一个用于将每次二元操作的中间结果与结果容器的值进行操作并重新设置结果容器的累积器 accumulator
- 一个用于对Stream元素和中间结果进行操作的二元操作符 combiner
- 一个用于对结果容器进行最终聚合的转换器 finisher(非必须)
```
/**
     * Simple implementation class for {@code Collector}.
     *
     * @param <T> the type of elements to be collected
     * @param <R> the type of the result
     */
static class CollectorImpl<T, A, R> implements Collector<T, A, R> {
        private final Supplier<A> supplier;
        private final BiConsumer<A, T> accumulator;
        private final BinaryOperator<A> combiner;
        private final Function<A, R> finisher;
        private final Set<Characteristics> characteristics;
        CollectorImpl(Supplier<A> supplier,
                      BiConsumer<A, T> accumulator,
                      BinaryOperator<A> combiner,
                      Function<A,R> finisher,
                      Set<Characteristics> characteristics) {
            this.supplier = supplier;
            this.accumulator = accumulator;
            this.combiner = combiner;
            this.finisher = finisher;
            this.characteristics = characteristics;
        }
        CollectorImpl(Supplier<A> supplier,
                      BiConsumer<A, T> accumulator,
                      BinaryOperator<A> combiner,
                      Set<Characteristics> characteristics) {
            this(supplier, accumulator, combiner, castingIdentity(), characteristics);
        }
}
```
toList()执行操作
```
    /**
     * Returns a {@code Collector} that accumulates the input elements into a
     * new {@code List}. There are no guarantees on the type, mutability,
     * serializability, or thread-safety of the {@code List} returned; if more
     * control over the returned {@code List} is required, use {@link #toCollection(Supplier)}.
     *
     * @param <T> the type of the input elements
     * @return a {@code Collector} which collects all the input elements into a
     * {@code List}, in encounter order
     */
    public static <T>
    Collector<T, ?, List<T>> toList() {
        return new CollectorImpl<>((Supplier<List<T>>) ArrayList::new, List::add,
                                   (left, right) -> { left.addAll(right); return left; },
                                   CH_ID);
    }
```
- 第一个参数是初始化数据通过ArrayList::new创建
- 第二个参数是执行的操作，list的add元素添加操作
- 第三个参数是执行最后的聚合操作

##### 映射类聚合器
```
private static <K, V, M extends Map<K,V>>
    BinaryOperator<M> mapMerger(BinaryOperator<V> mergeFunction) {
        return (m1, m2) -> {
            for (Map.Entry<K,V> e : m2.entrySet())
                m1.merge(e.getKey(), e.getValue(), mergeFunction);
            return m1;
        };
    }
```
根据指定的值合并函数 mergeFunction, 返回一个map合并器，用来合并两个map里相同key的值。mergeFunction用来对两个map中相同key的值进行运算得到新的value值，如果value值为null，会移除相应的key，否则使用value值作为对应key的值。这个方法是私有的，主要为支撑 toMap，groupingBy 而生。

##### 自定义聚合器
需要实现Collector，构造四要素：
- 可变的结果容器Supplier:初始化的参数，注意不能初始化不可变的：supplier
- 累积器 BiConsumer:使用参数来进行和终止，新元素放入结果容器中：accumulator()
- 组合器 BInaryOperator ：目前无作用：combiner()
- 最终转换器 Funcation: 最终返回数据模型：finisher() 
```
public class CollectorsImpls implements Collector<Integer, List<Integer>, List<Integer>> {
    /**
     * A function that creates and returns a new mutable result container.
     *
     * @return a function which returns a new, mutable result container
     */
    @Override
    public Supplier<List<Integer>> supplier() {
        return () -> {
            List<Integer> res = new ArrayList<>();
            res.add(0);
            res.add(1);
            return res;
        };
    }

    /**
     * A function that folds a value into a mutable result container.
     *
     * @return a function which folds a value into a mutable result container
     */
    @Override
    public BiConsumer<List<Integer>, Integer> accumulator() {
        return (res, num) -> {
            Integer next = res.get(res.size() - 1) + res.get(res.size() - 2);
            res.add(next);
        };
    }

    /**
     * A function that accepts two partial results and merges them.  The
     * combiner function may fold state from one argument into the other and
     * return that, or may return a new result container.
     *
     * @return a function which combines two partial results into a combined
     * result
     */
    @Override
    public BinaryOperator<List<Integer>> combiner() {

        return (res1, res2) -> {
            Integer num = res1.get(res1.size()) + res2.get(res2.size());
            res1.add(num);
            return res1;
        };
    }

    /**
     * Perform the final transformation from the intermediate accumulation type
     * {@code A} to the final result type {@code R}.
     *
     * <p>If the characteristic {@code IDENTITY_TRANSFORM} is
     * set, this function may be presumed to be an identity transform with an
     * unchecked cast from {@code A} to {@code R}.
     *
     * @return a function which transforms the intermediate result to the final
     * result
     */
    @Override
    public Function<List<Integer>, List<Integer>> finisher() {
        return res -> {
            res.remove(0);
            res.remove(1);
            return res;
        };
    }

    /**
     * Returns a {@code Set} of {@code Collector.Characteristics} indicating
     * the characteristics of this Collector.  This set should be immutable.
     *
     * @return an immutable set of collector characteristics
     */
    @Override
    public Set<Characteristics> characteristics() {
        return Collections.emptySet();
    }
}

```
```
List<Integer> fibo = Arrays.asList(1,1,1,1).stream().collect(new CollectorsImpls());
```
#### 流
##### Stream流接口
Stream 主要四类接口
- 流与流之间的转换：
  - filter(过滤)
  - map(映射转换),
  - mapToInt|Long|Double, flatMap(高维结构平铺)
  - flatMapTo[Int|Long|Double] 
  - sorted(排序)
  - distinct(不重复值)
  - peek(执行某种操作，流不变，可用于调试)
  - limit(限制到指定元素数量)
  - skip(跳过若干元素
- 流与值的转化
  - toArray（转为数组）
  - reduce（推导结果）
  - collect（聚合结果）
  - min(最小值)
  - max(最大值)
  - count (元素个数)
  - anyMatch (任一匹配)
  - allMatch(所有都匹配)
  - noneMatch(一个都不匹配)
  - findFirst（选择首元素）
  - findAny(任选一元素)
- 遍历
  - forEach(不保序遍历，比如并行流)
  - forEachOrdered（保序遍历)
- 构造流
  - empty (构造空流)
  - of (单个元素的流及多元素顺序流)
  - iterate (无限长度的有序顺序流)
  - generate (将数据提供器转换成无限非有序的顺序流)
  - concat (流的连接)
  - Builder (用于构造流的Builder对象)
##### 流的主要类型
- Reference（对象流）
- IntStream（int元素流）
- LongStream（long元素流）
- DoubleStream （double元素流）
##### collector实现
```
public final <R, A> R collect(Collector<? super P_OUT, A, R> collector) {
        A container;
        if (isParallel()
                && (collector.characteristics().contains(Collector.Characteristics.CONCURRENT))
                && (!isOrdered() || collector.characteristics().contains(Collector.Characteristics.UNORDERED))) {
            container = collector.supplier().get();
            BiConsumer<A, ? super P_OUT> accumulator = collector.accumulator();
            forEach(u -> accumulator.accept(container, u));
        }
        else {
            container = evaluate(ReduceOps.makeRef(collector));
        }
        return collector.characteristics().contains(Collector.Characteristics.IDENTITY_FINISH)
               ? (R) container
               : collector.finisher().apply(container);
    }
```
主要的重要的操作
```
public static <T, I> TerminalOp<T, I>
    makeRef(Collector<? super T, I, ?> collector) {
        Supplier<I> supplier = Objects.requireNonNull(collector).supplier();
        BiConsumer<I, ? super T> accumulator = collector.accumulator();
        BinaryOperator<I> combiner = collector.combiner();
        class ReducingSink extends Box<I>
                implements AccumulatingSink<T, I, ReducingSink> {
            @Override
            public void begin(long size) {
                state = supplier.get();
            }

            @Override
            public void accept(T t) {
                accumulator.accept(state, t);
            }

            @Override
            public void combine(ReducingSink other) {
                state = combiner.apply(state, other.state);
            }
        }
        return new ReduceOp<T, I, ReducingSink>(StreamShape.REFERENCE) {
            @Override
            public ReducingSink makeSink() {
                return new ReducingSink();
            }

            @Override
            public int getOpFlags() {
                return collector.characteristics().contains(Collector.Characteristics.UNORDERED)
                       ? StreamOpFlag.NOT_ORDERED
                       : 0;
            }
        };
    }
```
```
 final <R> R evaluate(TerminalOp<E_OUT, R> terminalOp) {
        assert getOutputShape() == terminalOp.inputShape();
        if (linkedOrConsumed)
            throw new IllegalStateException(MSG_STREAM_LINKED);
        linkedOrConsumed = true;

        return isParallel()
               ? terminalOp.evaluateParallel(this, sourceSpliterator(terminalOp.getOpFlags()))
               : terminalOp.evaluateSequential(this, sourceSpliterator(terminalOp.getOpFlags()));
    }
```
Box 是一个结果值的持有者； ReducingSink 用begin, accept, combine 三个方法定义了要进行的计算；ReducingSink是有状态的流数据消费的计算抽象，阅读Sink接口文档可知。ReduceOps.makeRef(collector) 返回了一个封装了Reduce操作的ReduceOps对象。注意到，这里都是声明要执行的计算，而不涉及计算的实际过程。展示了表达与执行分离的思想。真正的计算过程启动在 ReferencePipeline.evaluate 方法里

```
    /**
     * Evaluate the pipeline with a terminal operation to produce a result.
     *
     * @param <R> the type of result
     * @param terminalOp the terminal operation to be applied to the pipeline.
     * @return the result
     */
    final <R> R evaluate(TerminalOp<E_OUT, R> terminalOp) {
        assert getOutputShape() == terminalOp.inputShape();
        if (linkedOrConsumed)
            throw new IllegalStateException(MSG_STREAM_LINKED);
        linkedOrConsumed = true;

        return isParallel()
               ? terminalOp.evaluateParallel(this, sourceSpliterator(terminalOp.getOpFlags()))
               : terminalOp.evaluateSequential(this, sourceSpliterator(terminalOp.getOpFlags()));
    }
```
```
@Override
    final <P_IN> void copyInto(Sink<P_IN> wrappedSink, Spliterator<P_IN> spliterator) {
        Objects.requireNonNull(wrappedSink);

        if (!StreamOpFlag.SHORT_CIRCUIT.isKnown(getStreamAndOpFlags())) {
            wrappedSink.begin(spliterator.getExactSizeIfKnown());
            spliterator.forEachRemaining(wrappedSink);
            wrappedSink.end();
        }
        else {
            copyIntoWithCancel(wrappedSink, spliterator);
        }
    }
```
Spliterator 用来对流中的元素进行分区和遍历以及施加Sink指定操作，可以用于并发计算。Spliterator的具体实现类定义在 Spliterators 的静态类和静态方法中。其中有：
```
数组Spliterator:
static final class ArraySpliterator<T> implements Spliterator<T>
static final class IntArraySpliterator implements Spliterator.OfInt
static final class LongArraySpliterator implements Spliterator.OfLong
static final class DoubleArraySpliterator implements Spliterator.OfDouble

迭代Spliterator:
static class IteratorSpliterator<T> implements Spliterator<T>
static final class IntIteratorSpliterator implements Spliterator.OfInt
static final class LongIteratorSpliterator implements Spliterator.OfLong
static final class DoubleIteratorSpliterator implements Spliterator.OfDouble

抽象Spliterator:
public static abstract class AbstractSpliterator<T> implements Spliterator<T>
private static abstract class EmptySpliterator<T, S extends Spliterator<T>, C>
public static abstract class AbstractIntSpliterator implements Spliterator.OfInt
public static abstract class AbstractLongSpliterator implements Spliterator.OfLong
public static abstract class AbstractDoubleSpliterator implements Spliterator.OfDouble
```
每个具体的类都是实现了trySplit, forEachRemaining,tryAdvance,estimateSize,characteriscs,getComparator.
- trypSplit：用于流拆分，提供并发能力
- forEachRemaining：遍历流中的数据
- tryAdvance：消费流数据
整体的流程：
- Collector定义必要的聚合操作函数
- ReduceOps.makeRef 将 Collector 封装成一个计算对象 ReduceOps ，依赖的 ReducingSink 定义了具体的流数据消费过程
- Spliterator 用于对流中的元素进行分区和遍历以及施加Sink指定的操作
##### Pipeline
类 java.util.stream.AbstractPipeline 的方法 sourceSpliterator 拿到并且这里的 sourceStage 是一个 AbstractPipeline。 Pipeline 是实现流式计算的流水线抽象，也是Stream的实现类。可以看到，java.util.stream 定义了四种 pipeline: DoublePipeline, IntPipeline, LongPipeline, ReferencePipeline。
```
@Override
    public final Stream<P_OUT> filter(Predicate<? super P_OUT> predicate) {
        Objects.requireNonNull(predicate);
        return new StatelessOp<P_OUT, P_OUT>(this, StreamShape.REFERENCE,
                                     StreamOpFlag.NOT_SIZED) {
            @Override
            Sink<P_OUT> opWrapSink(int flags, Sink<P_OUT> sink) {
                return new Sink.ChainedReference<P_OUT, P_OUT>(sink) {
                    @Override
                    public void begin(long size) {
                        downstream.begin(-1);
                    }

                    @Override
                    public void accept(P_OUT u) {
                        if (predicate.test(u))
                            downstream.accept(u);
                    }
                };
            }
        };
    }
```
套路基本一样，关键点在于 accept 方法。filter 只在满足条件时将值传给下一个 pipeline, 而 map 将计算的值传给下一个 pipeline. StatelessOp 没有什么逻辑，JDK文档解释是：Base class for a stateless intermediate stage of a Stream。相应还有一个 StatefulOp, Head。 这些都是 ReferencePipeline ，负责将值在 pipeline 之间传递，交给 Sink 去计算
```
static class Head<E_IN, E_OUT> extends ReferencePipeline<E_IN, E_OUT>
abstract static class StatelessOp<E_IN, E_OUT> extends ReferencePipeline<E_IN, E_OUT>
abstract static class StatefulOp<E_IN, E_OUT> extends ReferencePipeline<E_IN, E_OUT>
```