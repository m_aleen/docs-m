---
title: 跨域问题
---
### 跨域问题
#### 什么是跨域
javaScript出于安全考虑，对不在同一页面的对象不允许操作
#### 跨域的场景
- 不同的二级域名
- 不同的协议
- 不同的端口号
#### 解决跨域问题
- JSONP（废弃）
jsonp通过带有回调函数callback的json来解决跨域问题。但是他支持的仅仅是get请求，其他的请求类型是不支持
- CORS
CORS（跨域资源共享），屏蔽开发着，通过浏览器自动添加头信息处理，用户无感知。服务端相应的也需要添加相关的头信息。它是会先请求OPTIONS的一个预请求，获取得到跨域支持的http方法，然后确认真正的请求。缺点是部分浏览器并不能支持该方式
- 中间层搭建
java或者node.js来搭建中间层，进行请求转发。缺点是中间多了一层转发对系统消耗和网络开销都是有很大的开销
- Nginx反向代理
需要搭建一个nginx反向代理服务器，在url请求时进行url转发具体服务器

#### CORS跨域解决
##### CORS涉及的请求响应头
- Access-Control-Allow-Origin允许客户端访问，支持*或者完整域名两种方式，并且不支持同时配置多个域名
- Access-Control-Allow-Methods允许访问的请求方式
- Access-Control-Allow-Credentials是否允许请求带有验证信息，若获取客户端下的cookie时需要设置为true
- Access-Control-Max-Age缓存配置
- Access-Control-Allow-Headers允许服务器访问的客户端请求头用逗号分隔。
```
public class CORSFilter implements Filter {  
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {  
        HttpServletResponse response = (HttpServletResponse) res;  
        response.setHeader("Access-Control-Allow-Origin", "*");  
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, PATCH");  
        response.setHeader("Access-Control-Max-Age", "3600");  
        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, Cache-control");  
        chain.doFilter(req, res);  
    }  
    public void init(FilterConfig filterConfig) {}  
    public void destroy() {}  
} 
```
```
@Configuration
public class ComponentRestAutoConfiguration {
    /**
     * rest 跨域配置
     */
    @Bean(initMethod="init")
    @ConfigurationProperties(prefix = ALLOWEDORIGIN_KEY_PREFIX)
    public AutoConfigInsideCorsFilter insideCorsFilter() {
        return new AutoConfigInsideCorsFilter();
    }
    
    /**
     * 默认allowedOrigins的属性的key的前缀
     */
    private static final String ALLOWEDORIGIN_KEY_PREFIX = "cors";
    
    @Provider
    class AutoConfigInsideCorsFilter extends InsideCorsFilter{
        private Map<String, String> origin;
        public Map<String, String> getOrigin() {
            return origin;
        }
        public void setOrigin(Map<String, String> origin) {
            this.origin = origin;
        }

        public void init() {
            if(MapUtils.isEmpty(origin)) {
                return ;
            }
            this.allowedOrigins = new HashSet<>(origin.values());
            this.origin = null;
        }
    }
}
```
