---
title: 你需要了解的JSON知识
---
**你需要了解的JSON知识**
----
*Json是什么？*
----
- JSON是一种数据交换模式，它被许都多的系统用于数据之间的交换。
- JSON独立于编程语言，虽然的JSON（JavaScript Object Notation）的英文感觉是和JavaScript是息息相关的，但是你用自己的编程语言是完全没有问题的。而且你并不需要先学习JavaScript。
- JSON拥有很好的可移植性。
*Json的基本语法*
----
- 名称-值对
如果是有编程基础的人会对这个很了解的，键值对的形式非常的多（例如：大家非常熟悉的java语言，最典型的就是java的map）。
非常简单的例子，例如：

    ```
    {
         "name": "M",
          "age": 18
    }
    ```
 简单的name，age就是名称，值就是M，18。这就是一个简单的json，有些需要注意的下面给大家慢慢讲。
 - json的正确语法
上面的实例中可以看出，名称是必须要有双引号，如果你了解的话，就可以看出如果没有上引号就和JavaScript相同了。下面就是整个json中的一些符号的作用：
    - { ： 开始读对象
    - } ： 结束读对象
    - [ ： 开始读数组
    - ] ： 结束读数组
    - : ：名称和值的分离
    - , ：新的一部分的开始
使用中其实并没有对键值中的命名有过多的要求，当然可以包括空格等一些特殊字符，由于我们需要发挥它的可移植性一般情况下我们是命名是不会加一些特殊的字符和空格的。

*Json的数据类型*
----
- 对象
    - 下面就是一个很简单的例子，最高一级的名称是person，它对应的是有两个数据的对象值。
    ```
        
    {
      "person": {
        "name": "M",
        "age": 18
      },
      "persion": {
        "name": "Q",
        "age": 20
      }
}
    ```
- 字符串
    - 字符串是其实上面的每个例子都是有提到的，那就是name这个属性，所以就不在这进行写例子了，字符串需要注意到的是在写的时候你可能会有些些特殊的字符，一般来说是不建议写的但是也是有办法可以写的那就是加入转译符。
    ```
    \/  正斜线
    \b  退格符
    \f  换页符
    \t  制表符
    \n  换行符
    \r  回车符
    \u  后面跟16进制的字符
    ```
    
- 数字
    -  整型
    -  浮点型
    ```
    {
        "a": 12,
        "b": 12.5,
        "c": -12
    }
    ```
- 布尔值
    - false
    - true
- null
    - null代表的没有值不要和JavaScript的undefined混淆，并且null必须是小写的,记得不要用上引号，不然那就是普通的字符串了
- 数组
    - 索引是在0开始的。
    - 数值中可以存放不同的数据类型
    
        ```
        {
           "names": [
           "Bob",
           "Allen",
           "Hello"
         ],
           "ages": [
           12,
           23,
           34
       ],
           "other": [
          "name",
          2,
         null
  ]
}
        ```
*结束语*
----
Json的简单的简绍今天就到这里，以有时间会给在写写JSON Schema，Json的校验机制，他会对数据的输入和参数类型进行一个校验。
