---
title: Linux下安装jdk（Centos版）
---
**准备工作**
----
1. 下载jdk，选择你需要的版本。（[下载](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)）
2. linux环境（需要理解基本的命令）

**基本的命令**
-----
```
sudo                     //获得root权限
mkdir xxx                //创建XXX文件夹
mv file path             //移动file到path路径
tar -zcvf   xxx.tar.gz   //解压xxx文件
```


**开始创建安装位置**
-----
```
    //拥有root权限（需要输入你的用户密码）
    sudo              
    //这是我的安装目录，你可以根据自己想安装的位置进行创建
    mkdir /usr/java  
    //移动下载好的jdk到你想要安装的位置 
    mv jdk-8u112-linux-x64.tar.gz /usr/java/
    //解压tar.gz压缩包
    tar -zxvf jdk-8u112-linux-x64.tar.gz
    //设置java环境
    vim ~/.bashrc
    //在打开的文件里面编写如下文件（注意JAVA_HOME是你自己上面jdk的bin路径）
    export JAVA_HOME=/usr/java/jdk1.8.0_112
    export JRE_HOME=${JAVA_HOME}/jre
    export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
    export PATH=${JAVA_HOME}/bin:$PATH
    //生效jdk配置
    source ~/.bashrc
```
**查看是否生效**
----
```
    java -version
    //出现如下结果
    java version "1.8.0_112"
    Java(TM) SE Runtime Environment (build 1.8.0_112-b15)
    Java HotSpot(TM) 64-Bit Server VM (build 25.112-b15, mixed mode)
```

**多说点**
----
如果你看其他教程的，会发现好多都是使用如下命令中配置jdk环境，不要在意这些两种方式都是可以的。
```
    vim /etc/profile
```
