---
title: Hexo+github搭建自己的专属博客
---

*Hexo是什么？*
----

  简单的来说[hexo](https://hexo.io/docs/)是一个博客框架，它可以快速的帮助你搭建一个博客。它会将你写的博客快速的转换成静态文件。类似现在前后台分离使用的shepherd。

*安装环境*
----

它的安装需要一定的环境如下：

| 环境   | Node.js  | git  | npm  |
| -----:| --------:|-----:|------|
| 版本   | < 7.0    | 不限  | 不限 |
| 验证安装| node -v  | git --verison|npm -v |

一般情况你在安装的时候node.js和npm是在一起的不需要单独的下载。

*Node.js的安装*
----
  我只讲讲在mac和linux环境中的搭建，实在抱歉没时间去windows中去验证。（windows大家可以参考这个[Windows搭建node](http://www.runoob.com/nodejs/nodejs-install-setup.html)）

1. 如下图的[Node.js](https://nodejs.org/en/download/)的官方网站下载  
![enter image description here](http://upload-images.jianshu.io/upload_images/5862120-3b508a8d7437f074.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

2. 安装nvm在Mac环境下

        wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash

  来进行nvm安装提出这个东西完全是这东西是非常好的mac软件安装管理工具，例如Ubuntu的apt-get和centos的yum。

3. 安装node.js 和 npm
            
            nvm install 6.9.1    #仅仅需要这个
            nvm install ls       #看看当前你安装的有哪些版本
            nvm use 6.9.1        #使用哪个版本

  我们仅仅需要这样一个命令就能完成对node的安装。当然还有其他的非常好用的命令，有兴趣的可以自己去探索，在这里这些不重要，你只需要知道这一个就好。一般情况node和npm是集成的所以node成功npm也就成功了。

**安装 git**
----

1. 安装 git 

            sudo brew install git

  相信大家都不陌生就不都说了，git这东西估计开发人员都有在使用。就不在啰嗦了。不太清楚的可以google也可以使用文末我的邮箱来询问我。

2. 生成SSH key

            ssh-keygen -t rsa -C  "xxxx.163.com"

  其实"xxx.163.com"只是个邮箱你随意，一般和github的注册邮箱一样。然后在根目录下生成.ssh的文件夹。（centos是在/目录下，mac在你的用户的根目录）

如图：id_rsa是你的私匙，id_rsa.pub是你的公匙一会要在github中使用![enter image description here](http://upload-images.jianshu.io/upload_images/5862120-076cfd5cd46b6a98.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![enter image description here](http://upload-images.jianshu.io/upload_images/5862120-1545624450dfbab4.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

key就是在github上添加id_rsa.pub中内容的位置，title这个不重要你写什么也可以只要自己方便分辨就好。

**github上创建一个仓库**
----

- 下图就是你新建的仓库的命名（命名一定要和Owner名字相同然后多加了一个github.io的后缀，这点很重要。）
![enter image description here](http://upload-images.jianshu.io/upload_images/5862120-5061b3adfad7ee40.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 仓库创建成功后需要记得这个地址，当然你也可以在后面用到的时候在到github中查看![](http://upload-images.jianshu.io/upload_images/5862120-ee280afd00085373.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**查看的环境**
----
- npm是否成功运行
        
        npm -v
        
- node是否成功，并且版本是否低于7.0

        node -v
        
- github是否成功
        
        git --version

  如果每个都能显示出版本号，那恭喜你基本的环境已经完成，你马上就可以拥有自己的博客家园了，下面我就请出我们今天的主角Hexo。

**Hexo的安装**
----
- 这个仅仅需要一条命令
        
        npm install -g hexo-cli

- 是不是感觉简单到爆，就这样你的基本博客环境就已经搭建起来了。

**开始搭建博客**
---
 
-    

        hexo init   [name]      ##初始化仓库

- name是选择输入的，你可以输入你想的名字，然后初始化的东西就放在你的name下。
            
            cd name       ##进入name目录查看生成的目录
- 下图就是里面的文件目录，当前最重要的是_config.yml文件![](http://upload-images.jianshu.io/upload_images/5862120-6bcd2eb73b83f954.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

        vim _config.yml    ##进入到文件的编辑中

- 找到下图中位置将里面的东西添加到你的文件中，记得将repository中的地址换成上面让你记的那个地址，那是你的github仓库地址。![](http://upload-images.jianshu.io/upload_images/5862120-e0d7e3c262b42934.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**开始使用hexo**
----
- 使用generate来生成静态文件

        hexo generate

- 使用deploy来提交到github上，就是将其部署。

        hexo deploy

- 此时你已经成功的部署到线上了，你可以使用http://mawenqiang.github.io来进行访问。

- 本地调试执行下面的命令，然后使用http://localhost:4000来进行本地的访问

        hexo server

**经常使用的命令**
----
- 下面的命令是进出需要用到的

        hexo generate       ##生成静态文件
        hexo deploy         ##部署到线上
        hexo sever          ##本地查看
        hexo new "新的文章名" ##新建文章

- 新文件存放在 source/_posts文件中

**问题**
---
- 在部署的时候可能会遇到很多问题，如果解决不了可以将问题发送到我的邮箱，这里就不一一的列举问题了。
- hexo中还有很多的命令可以使用，有兴趣的可以自己探索一下。
